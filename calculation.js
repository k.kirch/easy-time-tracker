const util = require('./util.js');
const moment = util.moment;

function calcDuration(startedAt, stoppedAt) {
    const startTime = moment(startedAt);
    const stopTime = moment(stoppedAt);
    const diff = moment('00:00:00', 'HH:mm:ss');
    diff.hours(stopTime.hours() - startTime.hours());
    diff.minutes(stopTime.minutes() - startTime.minutes());
    diff.seconds(stopTime.seconds() - startTime.seconds());
    return diff;
}

function roundDuration(_duration) {
    const duration = moment(_duration);
    if (duration.minutes() > 0) {
        duration.minutes(duration.minutes() + 1);
    }
    return duration;
}

function addTime(date1, date2) {
    date1.hours(date1.hours() + date2.hours());
    date1.minutes(date1.minutes() + date2.minutes());
    date1.seconds(date1.seconds() + date2.seconds());
    return date1;
}

function subtractTime(date1, date2) {
    date1.hours(date1.hours() - date2.hours());
    date1.minutes(date1.minutes() - date2.minutes());
    date1.seconds(date1.seconds() - date2.seconds());
    return date1;
}

function sumTimeOf(items) {
    let total = moment('00:00:00', util.timeFormat);
    items.forEach(item => {
        const duration = calcDuration(item.startedAt, item.stoppedAt);
        total = addTime(total, duration);
    });
    return total;
}

function sumWorkTimeOf(items) {
    let total = moment('00:00:00', util.timeFormat);
    items.forEach(item => {
        total = addTime(total, calcWorkDurationOf(item));
    });
    return total;
}

function calcWorkDurationOf(item) {
    let stoppedAt = moment();
    if (item.stoppedAt) {
        stoppedAt = item.stoppedAt;
    }
    return subtractTime(calcDuration(item.startedAt, stoppedAt), calcBreakDurationOf(item));
}

function calcBreakDurationOf(item) {
    return sumTimeOf(item.breaks || []);
}

module.exports = {
    util,
    calcDuration,
    roundDuration,
    addTime,
    subtractTime,
    sumTimeOf,
    sumWorkTimeOf,
    calcBreakDurationOf,
    calcWorkDurationOf
};