# easy-time-tracker

The Easy Time Tracker is a simple CLI to manage your work time.

## Use the Easy Time Tracker

Install the npm package with `npm install -g easy-time-tracker`

## Getting started

To start the tracking, run `ttrack start -j <Job Name>`.
To stop tracking, run `ttrack stop`.
If you want to have a break, run `ttrack pause` and resume work with `ttrack play`.
To add an entry, run `ttrack add -j <Job Name> -t "1-1-2020" "8:00" "17:00"`.  
To get more help, run `ttrack --help`.

## Data

The Data is saved in your home dir as a json file called `easy-time-tracker-store.json`.
It is recommended to backup this file once a month.

## Develop the Easy Time Tracker

The Easy Time Tracker is a small Node.js Application.
To develop on your machine, just run `npm link` inside the project root.
Run test Unit Tests with `npm run test`.
