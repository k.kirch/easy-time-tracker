const assert = require('assert');
const calc = require('../calculation.js');
const util = calc.util;
const moment = util.moment;

describe('Duration Calculation', () => {
    it('should calculation duration between 13:30:00 and 14:30:00', () => {
        const from = moment('13:30:00', util.timeFormat);
        const to = moment('14:30:00', util.timeFormat);
        assert.equal(calc.calcDuration(from, to).format(util.timeFormat), "01:00:00");
    });
    it('should calculation duration between 13:30:00 and 14:12:00', () => {
        const from = moment('13:30:00', util.timeFormat);
        const to = moment('14:12:00', util.timeFormat);
        assert.equal(calc.calcDuration(from, to).format(util.timeFormat), "00:42:00");
    });
    it('should calculation duration between 13:30:50 and 14:12:00', () => {
        const from = moment('13:30:50', util.timeFormat);
        const to = moment('14:12:00', util.timeFormat);
        assert.equal(calc.calcDuration(from, to).format(util.timeFormat), "00:41:10");
    });
    it('should calculation duration between 1-1-2020 23:30:00 and 2-1-2020 01:10:00', () => {
        const from = moment('1-1-2020 23:30', util.dateTimeFormat);
        const to = moment('2-1-2020 01:10', util.dateTimeFormat);
        assert.equal(calc.calcDuration(from, to).format(util.timeFormat), "01:40:00");
    });
    it('should round calculation duration between 13:30:31 and 14:30:02 to minutes', () => {
        const from = moment('13:30:31', util.timeFormat);
        const to = moment('14:30:02', util.timeFormat);
        const duration = calc.calcDuration(from, to);
        assert.equal(calc.roundDuration(duration).format(util.simpleTimeFormat), "01:00");
    });
    it('should round calculation duration between 13:30:00 and 14:30:00 to minutes', () => {
        const from = moment('13:30:00', util.timeFormat);
        const to = moment('14:30:00', util.timeFormat);
        const duration = calc.calcDuration(from, to);
        assert.equal(calc.roundDuration(duration).format(util.simpleTimeFormat), "01:00");
    });
});

describe('Time Calculation', () => {
    it('should add 1:20:00 to 2:30:00', () => {
        const from = moment('1:20:00', util.timeFormat);
        const to = moment('2:30:00', util.timeFormat);
        assert.equal(calc.addTime(from, to).format(util.timeFormat), "03:50:00");
    });
    it('should subtract 1:20:00 from 2:30:00', () => {
        const from = moment('1:20:00', util.timeFormat);
        const to = moment('2:30:00', util.timeFormat);
        assert.equal(calc.subtractTime(to, from).format(util.timeFormat), "01:10:00");
    });
    it('should sum duration of 1 item', () => {
        const items = [{
            startedAt: moment("1-1-2020 13:00:00", util.dateTimeFormat).format(),
            stoppedAt: moment("1-1-2020 15:00:00", util.dateTimeFormat).format()
        }];
        assert.equal(calc.sumTimeOf(items).format(util.timeFormat), "02:00:00");
    });
    it('should sum duration of 2 items', () => {
        const items = [{
            startedAt: moment("1-1-2020 13:00:05", util.dateTimeFormat).format(),
            stoppedAt: moment("1-1-2020 15:00:00", util.dateTimeFormat).format()
        }, {
            startedAt: moment("1-1-2020 16:01:25", util.dateTimeFormat).format(),
            stoppedAt: moment("1-1-2020 17:21:05", util.dateTimeFormat).format()
        }];
        assert.equal(calc.sumTimeOf(items).format(util.timeFormat), "03:19:35");
    });
    it('should return 0 for sum of 0 items', () => {
        const items = [];
        assert.equal(calc.sumTimeOf(items).format(util.timeFormat), "00:00:00");
    });
    it('should calc breaks duration of item', () => {
        const item = {
            breaks: [{
                startedAt: moment("1-1-2020 13:00:00", util.dateTimeFormat).format(),
                stoppedAt: moment("1-1-2020 15:00:00", util.dateTimeFormat).format()
            }, {
                startedAt: moment("1-1-2020 17:00:00", util.dateTimeFormat).format(),
                stoppedAt: moment("1-1-2020 17:10:00", util.dateTimeFormat).format()
            }]
        };
        assert.equal(calc.calcBreakDurationOf(item).format(util.timeFormat), "02:10:00");
    });
    it('should calc work duration of item with break', () => {
        const item = {
            startedAt: moment("1-1-2020 8:00:00", util.dateTimeFormat).format(),
            stoppedAt: moment("1-1-2020 17:00:00", util.dateTimeFormat).format(),
            breaks: [{
                startedAt: moment("1-1-2020 13:00:00", util.dateTimeFormat).format(),
                stoppedAt: moment("1-1-2020 15:00:00", util.dateTimeFormat).format()
            }]
        };
        assert.equal(calc.calcWorkDurationOf(item).format(util.timeFormat), "07:00:00");
    });
    it('should sum workTime of 2 items', () => {
        const items = [{
            startedAt: moment("1-1-2020 13:00:00", util.dateTimeFormat).format(),
            stoppedAt: moment("1-1-2020 15:00:00", util.dateTimeFormat).format(),
            breaks: [{
                startedAt: moment("1-1-2020 15:01:00", util.dateTimeFormat).format(),
                stoppedAt: moment("1-1-2020 15:30:00", util.dateTimeFormat).format(),
            }]
        }, {
            startedAt: moment("1-1-2020 16:00:00", util.dateTimeFormat).format(),
            stoppedAt: moment("1-1-2020 17:00:00", util.dateTimeFormat).format()
        }];
        assert.equal(calc.sumWorkTimeOf(items).format(util.timeFormat), "02:31:00");
    });
});

describe('Print Utils', () => {
    it('should print datetime 1-1-2020 13:30:00 to 13:30', () => {
        const time = moment('1-1-2020 13:30:00', util.dateTimeFormat);
        assert.equal(util.printableTime(time), "13:30");
        const timeString = moment('1-1-2020 13:30:00', util.dateTimeFormat).format();
        assert.equal(util.printableTime(timeString), "13:30");
    });
    it('should format column left', () => {
        const value = "Hello World";
        assert.equal(util.formatColumn(value, 20, 'LEFT'), "Hello World         ");
    });
    it('should format column right', () => {
        const value = "Hello World";
        assert.equal(util.formatColumn(value, 20, 'RIGHT'), "         Hello World");
    });
    it('should format column center', () => {
        const value = "Hello World";
        assert.equal(util.formatColumn(value, 20, 'CENTER'), "    Hello World     ");
    });
});

describe('Parser Utils', () => {
    it('should check for valid format', () => {
        assert.equal(util.isValidFormat("13:30", util.simpleTimeFormat), true);
        assert.equal(util.isValidFormat("13:30:00", util.simpleTimeFormat), false);
        assert.equal(util.isValidFormat(null, util.simpleTimeFormat), false);

        assert.equal(util.isValidFormat("1-1-2020 13:30", util.simpleTimeFormat), false);
        assert.equal(util.isValidFormat("1-1-2020 13:30:30", util.simpleTimeFormat), false);

        assert.equal(util.isValidFormat("13:30", util.timeFormat), false);
        assert.equal(util.isValidFormat("13:30:00", util.timeFormat), true);

        assert.equal(util.isValidFormat("13:30", util.dateFormat), false);
        assert.equal(util.isValidFormat("01-01-2020", util.dateFormat), true);
        assert.equal(util.isValidFormat("01-01-2020 13:00", util.dateFormat), false);

        assert.equal(util.isValidFormat("13:30", util.inputDateFormat), false);
        assert.equal(util.isValidFormat("01-01-2020", util.inputDateFormat), true);
        assert.equal(util.isValidFormat("1-1-2020", util.inputDateFormat), true);
        assert.equal(util.isValidFormat("1-1-2020 13:00", util.inputDateFormat), false);

        assert.equal(util.isValidFormat("13:30", util.inputTimeFormat), true);
        assert.equal(util.isValidFormat("01-01-2020", util.inputTimeFormat), false);
        assert.equal(util.isValidFormat("1-1-2020", util.inputTimeFormat), false);
        assert.equal(util.isValidFormat("1:00", util.inputTimeFormat), true);
    });

    it('should parse 13:30 to 13:30:00', () => {
        const value = "13:30";
        assert.equal(util.parseTime(value), "13:30:00");
    });
    it('should throw exception if null', () => {
        assert.throws(() => util.parseTime(null), Error, "Error: Time 'null' is not valid.");
    });
    it('should throw exception if invalid format', () => {
        assert.throws(() => util.parseTime("01-01-2020 13:30:00"), Error, "Error: Time '01-01-2020 13:30:00' is not valid.");
    });
    it('should parse timeDefault 13:30 to 13:30:00', () => {
        const value = "13:30";
        assert.equal(util.parseTimeWithDefault(value), "13:30:00");
    });
    it('should return default time for null', () => {
        assert.notEqual(util.parseTimeWithDefault(null), null);
    });
    it('should return default time for invalid format', () => {
        assert.notEqual(util.parseTimeWithDefault("01-01-2020 13:30:00"), null);
    });
    it('should print 3 hrs 10 min for 03:10:00', () => {
        assert.equal(util.printableTimeShort(moment("03:10:00", "HH:mm:ss")), "3 hrs 10 min");
    });
    it('should print 3 hrs and 10 minutes for 03:10:00', () => {
        assert.equal(util.printableTimeLong(moment("03:10:00", "HH:mm:ss")), "3 hrs and 10 minutes");
    });
    it('should parse 1-1-2020 to 01-01-2020', () => {
        const value = "1-1-2020";
        assert.equal(util.parseDate(value), "01-01-2020");
    });
    it('should throw exception if null', () => {
        assert.throws(() => util.parseDate(null), Error, "Error: Date 'null' is not valid.");
    });
    it('should throw exception if invalid format', () => {
        assert.throws(() => util.parseDate("01-01-2020 13:30:00"), Error, "Error: Date '01-01-2020 13:30:00' is not valid.");
    });
});