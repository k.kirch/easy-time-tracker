const fs = require("fs");
const os = require('os');
const storeFilename = "/easy-time-tracker-store.json";

function init() {
    return {
        entries: [],
        tracker: {
            state: "IDLE",
        },
    };
}

function loadStore(callback) {
    try {
        if (!fs.existsSync(os.homedir() + storeFilename)) {
            store = init();
            saveStore(store);
            callback(store);
        } else {
            fs.readFile(os.homedir() + storeFilename, (err, data) => {
                if (err) {
                    console.error("Failed to load Store", err);
                    return;
                }
                try {
                    store = JSON.parse(data);
                } catch (_) {
                    console.debug('Failed to parse Store', _);
                    store = init();
                }
                callback(store);
            });
        }
    } catch (err) {
        console.error("Failed to init store", err);
    }
}

function saveStore(store) {
    fs.writeFile(os.homedir() + storeFilename, JSON.stringify(store), function (err) {
        if (err) return console.error(err);
    });
}

module.exports = {
    init,
    loadStore,
    saveStore,
};