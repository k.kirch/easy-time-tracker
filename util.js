const moment = require('moment-timezone');
moment.locale('de');
moment.tz("Europe/Berlin");

const dateTimeFormat = "DD-MM-YYYY HH:mm:ss";
const simpleDateTimeFormat = "DD-MM-YYYY HH:mm";
const dateFormat = "DD-MM-YYYY";
const inputDateFormat = "D-M-YYYY";
const timeFormat = "HH:mm:ss";
const simpleTimeFormat = "HH:mm";
const inputTimeFormat = "H:m";
const weekFormat = "ww/YYYY";
const monthFormat = "MM-YYYY";

function printableTime(time) {
    return moment(time).format('HH:mm');
}

function printableTimeShort(_time) {
    const time = moment(_time);
    return time.hours() + " hrs " + time.minutes() + " min";
}

function printableTimeLong(_time) {
    const time = moment(_time);
    return time.hours() + " hrs and " + time.minutes() + " minutes";
}

function parseTimeWithDefault(_time) {
    let time = moment().format(timeFormat);
    if (_time && moment(_time, simpleTimeFormat).format() != 'Invalid date') {
        time = moment(_time, simpleTimeFormat).format(timeFormat);
    }
    return time;
}

function parseTime(_time) {
    if (!isValidFormat(_time, inputTimeFormat)) {
        throw new Error("Time '" + _time + "' is not valid.");
    }
    return moment(_time, inputTimeFormat).format(timeFormat);
}

function parseDate(_date) {
    if (!isValidFormat(_date, inputDateFormat)) {
        throw new Error("Date '" + _date + "' is not valid.");
    }
    return moment(_date, inputDateFormat).format(dateFormat);
}

function isValidFormat(value, format) {
    return value != null && moment(value, format, true).format() != 'Invalid date' && moment(value, format, true) != null;
}

function formatColumn(value, maxLength, pos = 'LEFT') {
    let _value = value;
    if (!value) {
        _value = "";
    }
    const diff = maxLength - _value.length;
    if (diff < 0) {
        return _value.substr(0, maxLength);
    }
    if (pos === 'LEFT') {
        return _value + new Array(diff + 1).join(' ');
    }
    if (pos === 'RIGHT') {
        return new Array(diff + 1).join(' ') + _value;
    }
    if (pos === 'CENTER') {
        return new Array(Math.floor(diff / 2) + 1).join(' ') + _value + new Array(Math.ceil(diff / 2) + 1).join(' ');
    }
    return '';
}

module.exports = {
    timeFormat,
    dateFormat,
    dateTimeFormat,
    simpleTimeFormat,
    weekFormat,
    monthFormat,
    inputDateFormat,
    inputTimeFormat,
    moment,
    isValidFormat,
    printableTime,
    parseTime,
    parseTimeWithDefault,
    printableTimeShort,
    printableTimeLong,
    formatColumn,
    parseDate
};