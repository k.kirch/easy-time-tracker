#!/usr/bin/env node

const util = require("./util.js");
const moment = util.moment;
const calc = require("./calculation.js");
const {
    program
} = require("commander");
const storeUtil = require("./store.js");

// set version of easy time tracker
program.version("1.0.0");
// declare cli
/* ### CLI ###
start <job> -> Starts the Time Tracker at Current Time for the Job if tracker not running.
start <job> <time> -> Starts the Time Tracker at given time for the job if tracker is not running.
stop -> Stops the Time tracker if running
list <day> -> List the Tracked Times of the Current Day
*/
program.option("--debug", "output extra debugging");
// handle commands
// start
program
    .command("start")
    .requiredOption("-j, --job <job>", "Tracker needs a job")
    .option("-t, --time <time>", "Time the Tracker starts at")
    .option("-q, --quit", "Quit the current tracker and start a new one")
    .description(
        "Starts the time tracker at given time or current time for the job if tracker not running."
    )
    .action((_program) => {
        storeUtil.loadStore((_store) => {
            start(_store, _program.job, _program.time, _program.quit);
        });
    });
// stop
program
    .command("stop")
    .option("-t, --time <time>", "Time the Tracker stops at")
    .description(
        "Stops the time tracker at given time or current time for the job if tracker is running."
    )
    .action((_program) => {
        storeUtil.loadStore((_store) => {
            stop(_store, _program.time);
        });
    });
// list
program
    .command("today")
    .option("-j, --job <job>", "filter list for job")
    .description("List all items of today")
    .action((_program) => {
        storeUtil.loadStore((_store) => {
            listToday(_store, _program.job);
        });
    });
program
    .command("week")
    .option("-w, --week <week>", "show exact week")
    .option("-y, --year <year>", "show exact week for year")
    .option("-j, --job <job>", "filter list for job")
    .description("List all items of week")
    .action((_program) => {
        storeUtil.loadStore((_store) => {
            listWeek(_store, _program.week, _program.year, _program.job);
        });
    });
program
    .command("month")
    .option("-m, --month <month>", "show exact month")
    .option("-y, --year <year>", "show for year")
    .option("-j, --job <job>", "filter list for job")
    .description("List all items of a month")
    .action((_program) => {
        storeUtil.loadStore((_store) => {
            listMonth(_store, _program.month, _program.year, _program.job);
        });
    });
// info
program
    .command("info")
    .description("show information")
    .action((_program) => {
        storeUtil.loadStore((_store) => {
            info(_store);
        });
    });
// breaks
program
    .command("pause")
    .option("-t, --time <time>", "Time the Tracker pause at")
    .description("start break")
    .action((_program) => {
        storeUtil.loadStore((_store) => {
            startBreak(_store, _program.time);
        });
    });
program
    .command("play")
    .option("-t, --time <time>", "Time the Tracker restart at")
    .description("stop break")
    .action((_program) => {
        storeUtil.loadStore((_store) => {
            stopBreak(_store, _program.time);
        });
    });
program
    .command("add")
    .requiredOption("-j, --job <job>", "Job")
    .option("-t, --time <date> <startTime> <endTime>", "Add time")
    .option("-d, --duration <date> <duration>", "Add duration")
    .description("Add a new entry")
    .action((_program) => {
        storeUtil.loadStore((_store) => {
            if (_program.time) {
                addTimeEntry(
                    _store,
                    _program.job,
                    _program.time,
                    _program.args[0],
                    _program.args[1]
                );
            } else if (_program.duration) {
                addDuration(_store, _program.job, _program.duration, _program.args[0]);
            } else {
                console.log(
                    "Please choose between -t and -d to add a time interval or a duration"
                );
            }
        });
    });

program.parse(process.argv);
// log detailed information for debugging purpose
if (program.debug) {
    console.debug(program.opts());
}

function addTimeEntry(store, job, _date, _startTime, _endTime) {
    const date = util.parseDate(_date);
    const startTime = util.parseTime(_startTime);
    const endTime = util.parseTime(_endTime);
    store.entries.push({
        startedAt: moment(
            date + " " + startTime,
            util.dateTimeFormat
        ).format(),
        stoppedAt: moment(
            date + " " + endTime,
            util.dateTimeFormat
        ).format(),
        job,
        index: store.entries.length,
    });
    console.log("Added Entry for %s from %s to %s", date, startTime, endTime);
    storeUtil.saveStore(store);
}

function addDuration(store, job, _date, _duration) {
    const date = util.parseDate(_date);
    const startTime = util.parseTime("8:00");
    const endTime = calc.addTime(moment(startTime, util.timeFormat), moment(util.parseTime(_duration), util.timeFormat)).format(util.timeFormat);
    store.entries.push({
        startedAt: moment(
            date + " " + startTime,
            util.dateTimeFormat
        ).format(),
        stoppedAt: moment(
            date + " " + endTime,
            util.dateTimeFormat
        ).format(),
        job,
        index: store.entries.length,
    });
    console.log("Added Entry for %s from %s to %s", date, startTime, endTime);
    storeUtil.saveStore(store);
}

function start(store, job, _time, quitRunningTracker) {
    const time = util.parseTimeWithDefault(_time);
    if (quitRunningTracker && store.tracker.state == "RUNNING") {
        stop(time);
    }
    if (store.tracker.state == "IDLE") {
        store.tracker.state = "RUNNING";
        store.tracker.job = job;
        store.tracker.startedAt = moment(time, util.timeFormat).format();
        console.log(
            'tracker started for job "%s" at "%s"',
            store.tracker.job,
            util.printableTime(store.tracker.startedAt)
        );
    } else {
        console.log(
            'tracker is already tracking job "%s" since %s. You can stop the current tracker with -q',
            store.tracker.job,
            util.printableTimeShort(store.tracker.startedAt)
        );
    }
    storeUtil.saveStore(store);
}

function startBreak(store, _time) {
    if (store.tracker.state == "IDLE") {
        console.log("No tracker running.");
        return;
    }
    const time = util.parseTimeWithDefault(_time);
    if (store.tracker.state == "RUNNING") {
        if (store.tracker.break) {
            console.log(
                "Tracker already paused at %s",
                util.printableTime(store.tracker.break.startedAt)
            );
            return;
        }
        const startBreakAt = moment(time, util.timeFormat);
        store.tracker.break = {
            startedAt: startBreakAt.format(),
        };
        console.log(
            'Tracker paused for job "%s" at %s',
            store.tracker.job,
            util.printableTime(store.tracker.break.startedAt)
        );
    }
    storeUtil.saveStore(store);
}

function stopBreak(store, _time) {
    if (store.tracker.state == "IDLE") {
        console.log("No tracker running.");
        return;
    }
    const time = util.parseTimeWithDefault(_time);
    if (store.tracker.state == "RUNNING") {
        if (!store.tracker.break) {
            console.log("No break to stop");
            return;
        }
        const startedBreakAt = moment(store.tracker.break.startedAt);
        const stopBreakAt = moment(time, util.timeFormat);
        if (!store.tracker.breaks) {
            store.tracker.breaks = [];
        }
        store.tracker.breaks.push({
            startedAt: startedBreakAt.format(),
            stoppedAt: stopBreakAt.format(),
        });
        delete store.tracker.break;
        console.log(
            'Tracker continued for job "%s" at %s',
            store.tracker.job,
            util.printableTime(stopBreakAt)
        );
    }
    storeUtil.saveStore(store);
}

function stop(store, _time) {
    if (store.tracker.state == "IDLE") {
        console.log("No tracker running.");
        return;
    }
    const time = util.parseTimeWithDefault(_time);
    if (store.tracker.state == "RUNNING") {
        const stoppedAt = moment(time, util.timeFormat);
        store.entries.push({
            index: store.entries.length,
            startedAt: store.tracker.startedAt,
            job: store.tracker.job,
            stoppedAt: stoppedAt.format(),
        });
        console.log(
            'Tracker stopped for job "%s". Total %s',
            store.tracker.job,
            util.printableTimeLong(
                calc.calcDuration(store.tracker.startedAt, stoppedAt)
            )
        );
        store.tracker.state = "IDLE";
        delete store.tracker.job;
        delete store.tracker.startedAt;
    }
    storeUtil.saveStore(store);
}

function info(store) {
    console.log("Total of today: %s", util.printableTimeLong(getTotalOfToday(store)));
    console.log(
        "Total of week: %s",
        util.printableTimeLong(getTotalOfCurrentWeek(store))
    );
    console.log(
        "Total of month: %s",
        util.printableTimeLong(getTotalOfCurrentMonth(store))
    );
    if (store.tracker.state == "IDLE") {
        console.log("No tracker running.");
    }
    if (store.tracker.state == "RUNNING") {
        console.log(
            'tracker is running for job "%s" since %s',
            store.tracker.job,
            util.printableTimeLong(
                calc.calcDuration(store.tracker.startedAt, moment())
            )
        );
    }
    if (store.tracker.breaks) {
        console.log("Breaks:");
        printBreaks(store.tracker.breaks);
    }
}

function getTotalOfToday(store) {
    const now = moment();
    let total = moment("00:00:00", "hh:mm:ss");
    if (store.tracker.state === "RUNNING") {
        total = calc.addTime(
            total,
            calc.calcDuration(store.tracker.startedAt, now)
        );
    }
    const itemsOfToday = store.entries.filter(
        (item) =>
        moment(item.startedAt).format(util.dateFormat) ===
        now.format(util.dateFormat)
    );
    total = calc.addTime(total, calc.sumWorkTimeOf(itemsOfToday));
    return total;
}

function getTotalOfCurrentWeek(store) {
    const now = moment();
    let total = moment("00:00:00", "hh:mm:ss");
    if (store.tracker.state === "RUNNING") {
        total = calc.addTime(
            total,
            calc.calcDuration(store.tracker.startedAt, now)
        );
    }
    const itemsOfWeek = store.entries.filter(
        (item) =>
        moment(item.startedAt).format(util.weekFormat) ===
        now.format(util.weekFormat)
    );
    total = calc.addTime(total, calc.sumWorkTimeOf(itemsOfWeek));
    return total;
}

function getTotalOfCurrentMonth(store) {
    const now = moment();
    let total = moment("00:00:00", "hh:mm:ss");
    if (store.tracker.state === "RUNNING") {
        total = calc.addTime(
            total,
            calc.calcDuration(store.tracker.startedAt, now)
        );
    }
    const itemsOfMonth = store.entries.filter(
        (item) =>
        moment(item.startedAt).format(util.monthFormat) ===
        now.format(util.monthFormat)
    );
    total = calc.addTime(total, calc.sumWorkTimeOf(itemsOfMonth));
    return total;
}

function listToday(store, job) {
    const now = moment();
    if (job) {
        console.log(
            'Items for %s filtered by "%s"',
            now.format(util.dateFormat),
            job
        );
    } else {
        console.log("Items for %s", now.format(util.dateFormat));
    }
    const items = store.entries.filter((item) => {
        return (
            ((job && item.job.match(job)) || !job) &&
            moment(item.startedAt).format(util.dateFormat) ===
            now.format(util.dateFormat)
        );
    });
    printList(store, items, true);
}

function listWeek(store, _week, _year, job) {
    const now = moment();
    let week = now.format("ww");
    let year = now.format("YYYY");
    if (_week) {
        week = moment(_week, "ww").format("ww");
    }
    if (_year) {
        year = moment(_year, "YYYY").format("YYYY");
    }
    if (job) {
        console.log('Items for KW %s/%s filtered by "%s"', week, year, job);
    } else {
        console.log("Items for KW %s/%s", week, year);
    }
    const items = store.entries.filter((item) => {
        return (
            ((job && item.job.match(job)) || !job) &&
            moment(item.startedAt).format(util.weekFormat) === week + "/" + year
        );
    });
    printList(store, items, week === now.format("ww") && year === now.format("YYYY"));
}

function listMonth(store, _month, _year, job) {
    const now = moment();
    let month = now.format("MM");
    let year = now.format("YYYY");
    if (_month) {
        month = moment(_month, "MM").format("MM");
    }
    if (_year) {
        year = moment(_year, "YYYY").format("YYYY");
    }
    if (job) {
        console.log(
            'Items for %s %s filtered by "%s"',
            moment(month, "MM").format("MMMM"),
            year,
            job
        );
    } else {
        console.log("Items for %s %s", moment(month, "MM").format("MMMM"), year);
    }
    const items = store.entries.filter((item) => {
        return (
            ((job && item.job.match(job)) || !job) &&
            moment(item.startedAt).format("MM-YYYY") === month + "-" + year
        );
    });
    printList(store, items, month === now.format("MM") && year === now.format("YYYY"));
}

function printList(store, items, appendCurrent = false) {
    console.log(
        "    Date     |             Job             |   Start   |     End    |    Breaks    |                Duration          "
    );
    console.log(
        "-------------|-----------------------------|-----------|------------|--------------|----------------------------------"
    );
    if (items.length === 0 || (!appendCurrent && store.tracker.state === "IDLE")) {
        console.log(util.formatColumn("No items found", 103, "CENTER"));
    }
    items
        .sort(
            (a, b) => moment(a.startedAt).valueOf() - moment(b.startedAt).valueOf()
        )
        .forEach((item) => {
            console.log(
                "  %s| %s| %s| %s| %s| %s",
                util.formatColumn(moment(item.startedAt).format(util.dateFormat), 11),
                util.formatColumn(item.job, 28),
                util.formatColumn(
                    moment(item.startedAt).format(util.simpleTimeFormat),
                    10,
                    "CENTER"
                ),
                util.formatColumn(
                    moment(item.stoppedAt).format(util.simpleTimeFormat),
                    11,
                    "CENTER"
                ),
                util.formatColumn(
                    util.printableTimeShort(calc.calcBreakDurationOf(item)),
                    13,
                    "CENTER"
                ),
                util.formatColumn(
                    util.printableTimeShort(calc.calcWorkDurationOf(item)),
                    30,
                    "RIGHT"
                )
            );
        });
    let total = calc.sumWorkTimeOf(items);
    if (appendCurrent === true && store.tracker.state === "RUNNING") {
        console.log(
            "* %s| %s| %s| %s| %s| %s",
            util.formatColumn(
                moment(store.tracker.startedAt).format(util.dateFormat),
                11
            ),
            util.formatColumn(store.tracker.job, 28),
            util.formatColumn(
                moment(store.tracker.startedAt).format(util.simpleTimeFormat),
                10,
                "CENTER"
            ),
            util.formatColumn("now", 11, "CENTER"),
            util.formatColumn(
                util.printableTimeShort(calc.calcBreakDurationOf(store.tracker)),
                13,
                "CENTER"
            ),
            util.formatColumn(
                util.printableTimeShort(
                    calc.calcWorkDurationOf(calc.calcWorkDurationOf(store.tracker))
                ),
                30,
                "RIGHT"
            )
        );
        total = calc.addTime(total, calc.calcWorkDurationOf(store.tracker));
    }
    console.log(
        "----------------------------------------------------------------------------------------------------------------------"
    );
    console.log(
        util.formatColumn("Total " + util.printableTimeShort(total), 115, "RIGHT")
    );
}

function printBreaks(breaks) {
    console.log("   Start   |     End    |          Duration        ");
    console.log("-----------|------------|--------------------------");
    breaks
        .sort(
            (a, b) => moment(a.startedAt).valueOf() - moment(b.startedAt).valueOf()
        )
        .forEach((_break) => {
            console.log(
                " %s| %s| %s",
                util.formatColumn(
                    moment(_break.startedAt).format(util.simpleTimeFormat),
                    10,
                    "CENTER"
                ),
                util.formatColumn(
                    moment(_break.stoppedAt).format(util.simpleTimeFormat),
                    11,
                    "CENTER"
                ),
                util.formatColumn(
                    util.printableTimeShort(
                        calc.calcDuration(_break.startedAt, _break.stoppedAt)
                    ),
                    24,
                    "RIGHT"
                )
            );
        });
    let total = calc.sumTimeOf(breaks);
    console.log("---------------------------------------------------");
    console.log(
        util.formatColumn("Total " + util.printableTimeShort(total), 50, "RIGHT")
    );
}